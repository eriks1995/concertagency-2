<?php

require "model.php";

if (!empty($_POST['rsubmit'])) {
    if (empty($_POST['bname'])) {
        echo '<script>';
        echo 'alert("Name is required!");';
        echo '</script>';
    } else if (empty($_POST['bdate'])) {
        echo '<script>';
        echo 'alert("Date is required!");';
        echo '</script>';
    } else if (empty($_POST['barrive'])) {
        echo '<script>';
        echo 'alert("Time is required!");';
        echo '</script>';
    } else if (empty($_POST['blength'])) {
        echo '<script>';
        echo 'alert("Duration is required!");';
        echo '</script>';
    } else if (empty($_POST['bnumber'])) {
        echo '<script>';
        echo 'alert("Amount is required!");';
        echo '</script>';
    } else if (empty($_POST['brname'])) {
        echo '<script>';
        echo 'alert("Your name is required!");';
        echo '</script>';
    } else if (empty($_POST['bron'])) {
        echo '<script>';
        echo 'alert("Reservation is required");';
        echo '</script>';
    } else {
        $bname = $_POST['bname'];
        $bdate = $_POST['bdate'];
        $barrive = $_POST['barrive'];
        $blength = $_POST['blength'];
        $bnumber = $_POST['bnumber'];
        $brname = $_POST['brname'];
        $bron = $_POST['bron'];
        $tel = $_POST['btel'];
        $comment = $_POST['bcom'];

        model_addReservation($bname,$bdate,$barrive,$blength,$bnumber,$brname,$bron,$tel,$comment);
        echo '<script>';
        echo 'alert("Your reservation successfully entered!");';
        echo '</script>';
    }
}