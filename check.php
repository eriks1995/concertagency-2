<?php
include("model.php");

$username = $_POST['username'];
$password = $_POST['password'];

if(model_getUser($username, $password)){
    $_SESSION['loggedin'] = true;
    $_SESSION['username'] = $username;
	header("location:secure.php");
} else {
	echo "Wrong Username or Password";
}
?>

<?php
include("footer.php");
?>

