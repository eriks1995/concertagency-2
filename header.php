<?php
require "model.php";
require "controller.php";
?>

<head>
    <meta charset="utf-8">
    <title>ConcertAgency</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="banner">
    <img src="images/banner.png">
</div><!-- banner ends -->
<div class="wrapper">
    <div class="header">
        <?php
        if (!isset($_SESSION['username'])) { ?>
            <div class="leftheader">
                <li class="nav-item"><a href="./index.php" class="nav">ConcertAgency</a></li>
                <li class="nav-item"><a href="./order.php" class="nav">OrderConcert</a></li>
                <li class="nav-item"><a href="./calendar.php" class="nav">Calendar</a></li>
            </div><!-- leftheader ends -->
        <?php } else {
            echo "<li class=\"nav-item\"><a href=\"./concert.php\" class=\"nav\">Concerts</a></li>";
            echo "<li class=\"nav-item\"><a href=\"./reservation.php\" class=\"nav\">Reservations</a></li>";
        }

        ?>
        <div class="rightheader">
            <?php
            if (!isset($_SESSION['username'])) {
                echo "<li class='nav-item'><a href='./adduser.php' class='nav'>AddUser</a></li>";
                echo "<li class='nav-item'><a href='./login.php' class='nav'>LogIn</a></li>";
            } else {
                echo "<li class='nav-item'><a href='./logout.php' class='nav'>LogOut</a></li>";
            }
            ?>

        </div><!-- rightheader ends -->
    </div><!-- rightheader ends -->





